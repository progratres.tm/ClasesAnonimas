package Ejemplo;

import java.util.ArrayList;

public class LasPersonas 
{
	private ArrayList<Persona> lasPersonas;
	
	public LasPersonas()
	{
		lasPersonas = new ArrayList<Persona>();
	}
	
	public void agregar(Persona p)
	{
		lasPersonas.add(p);
	}
	
	public int cantidad()
	{
		return lasPersonas.size();
	}
	
	public LasPersonas buscar(Condicion condicion)
	{
		LasPersonas ret = new LasPersonas();
		for (Persona p: lasPersonas)
			if (condicion.cumple(p))
				ret.agregar(p);
		return ret;
	}
	
	public Persona damePersona(int i)
	{
		return lasPersonas.get(i);
	}

	@Override
	public String toString() 
	{
		String ret = "";
		for (int i = 0; i < cantidad(); ++i)
			ret += damePersona(i).nombre() + " edad:" + damePersona(i).edad() + " " + damePersona(i).esCasado() + "\n";
		return ret;
	}
}
