package Ejemplo;

public class Persona 
{
	private String _nombre;
	private int _edad;
	private boolean _casado;
	
	public Persona(String nombre, int edad, boolean casado) {
		_nombre = nombre;
		_edad = edad;
		_casado = casado;
	}

	public String nombre() 
	{
		return _nombre;
	}

	public int edad() 
	{
		return _edad;
	}

	public boolean esCasado() 
	{
		return _casado;
	}
}
